/* GdkGLExt - OpenGL Extension to GDK
 * Copyright (C) 2002-2004  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#ifndef __GDK_GL_X_H__
#define __GDK_GL_X_H__

#include <gdk/gdkx.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <GL/gl.h>
#include <GL/glx.h>

#include <gdk/x11/gdkglglxext.h>

G_BEGIN_DECLS

gboolean      gdk_x11_gl_query_glx_extension      (GdkGLConfig  *gl_config,
                                                   const char   *extension);

#ifndef GDK_MULTIHEAD_SAFE
GdkGLConfig  *gdk_x11_gl_config_new_from_visualid            (VisualID   xvisualid);
#endif /* GDK_MULTIHEAD_SAFE */
#ifdef GDKGLEXT_MULTIHEAD_SUPPORT
GdkGLConfig  *gdk_x11_gl_config_new_from_visualid_for_screen (GdkScreen *screen,
                                                              VisualID   xvisualid);
#endif /* GDKGLEXT_MULTIHEAD_SUPPORT */

Display      *gdk_x11_gl_config_get_xdisplay      (GdkGLConfig  *gl_config);
int           gdk_x11_gl_config_get_screen_number (GdkGLConfig  *gl_config);
XVisualInfo  *gdk_x11_gl_config_get_xvinfo        (GdkGLConfig  *gl_config);
gboolean      gdk_x11_gl_config_is_mesa_glx       (GdkGLConfig  *gl_config);

GdkGLContext *gdk_x11_gl_context_foreign_new      (GdkGLConfig  *gl_config,
                                                   GdkGLContext *share_list,
                                                   GLXContext    glxcontext);

GLXContext    gdk_x11_gl_context_get_glxcontext   (GdkGLContext *gl_context);

GLXPixmap     gdk_x11_gl_pixmap_get_glxpixmap     (GdkGLPixmap  *gl_pixmap);

Window        gdk_x11_gl_window_get_glxwindow     (GdkGLWindow  *gl_window);

#ifdef INSIDE_GDK_GL_X11

#define GDK_GL_CONFIG_XDISPLAY(gl_config)       (GDK_GL_CONFIG_IMPL_X11 (gl_config)->xdisplay)
#define GDK_GL_CONFIG_SCREEN_XNUMBER(gl_config) (GDK_GL_CONFIG_IMPL_X11 (gl_config)->screen_num)
#define GDK_GL_CONFIG_XVINFO(gl_config)         (GDK_GL_CONFIG_IMPL_X11 (gl_config)->xvinfo)
#define GDK_GL_CONFIG_XCOLORMAP(gl_config)      (GDK_COLORMAP_XCOLORMAP (GDK_GL_CONFIG_IMPL_X11 (gl_config)->colormap))
#define GDK_GL_CONTEXT_GLXCONTEXT(gl_context)   (GDK_GL_CONTEXT_IMPL_X11 (gl_context)->glxcontext)
#define GDK_GL_PIXMAP_GLXPIXMAP(gl_pixmap)      (GDK_GL_PIXMAP_IMPL_X11 (gl_pixmap)->glxpixmap)
#define GDK_GL_WINDOW_GLXWINDOW(gl_window)      (GDK_GL_WINDOW_IMPL_X11 (gl_window)->glxwindow)

#else

#define GDK_GL_CONFIG_XDISPLAY(gl_config)       (gdk_x11_gl_config_get_xdisplay (gl_config))
#define GDK_GL_CONFIG_SCREEN_XNUMBER(gl_config) (gdk_x11_gl_config_get_screen_number (gl_config))
#define GDK_GL_CONFIG_XVINFO(gl_config)         (gdk_x11_gl_config_get_xvinfo (gl_config))
#define GDK_GL_CONFIG_XCOLORMAP(gl_config)      (GDK_COLORMAP_XCOLORMAP (gdk_gl_config_get_colormap (gl_config)))
#define GDK_GL_CONTEXT_GLXCONTEXT(gl_context)   (gdk_x11_gl_context_get_glxcontext (gl_context))
#define GDK_GL_PIXMAP_GLXPIXMAP(gl_pixmap)      (gdk_x11_gl_pixmap_get_glxpixmap (gl_pixmap))
#define GDK_GL_WINDOW_GLXWINDOW(gl_window)      (gdk_x11_gl_window_get_glxwindow (gl_window))

#endif

G_END_DECLS

#endif /* __GDK_GL_X_H__ */
