/* GdkGLExt - OpenGL Extension to GDK
 * Copyright (C) 2002-2004  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <gdk/gdkdrawable.h>

#include "gdkglprivate.h"
#include "gdkglcontext.h"
#include "gdkgldrawable.h"

GType
gdk_gl_drawable_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo type_info = {
        sizeof (GdkGLDrawableClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL
      };

      type = g_type_register_static (G_TYPE_INTERFACE,
                                     "GdkGLDrawable",
                                     &type_info, 0);
    }

  return type;
}

/**
 * gdk_gl_drawable_make_current:
 * @gl_drawable: a #GdkGLDrawable.
 * @gl_context: a #GdkGLContext.
 *
 * Attach an OpenGL rendering context to a @gl_drawable.
 *
 * Return value: TRUE if it is successful, FALSE otherwise.
 **/
gboolean
gdk_gl_drawable_make_current (GdkGLDrawable *gl_drawable,
                              GdkGLContext  *gl_context)
{
  g_return_val_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable), FALSE);

  return GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->make_context_current (gl_drawable,
                                                                        gl_drawable,
                                                                        gl_context);
}

/**
 * gdk_gl_drawable_is_double_buffered:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Returns whether the @gl_drawable supports the double-buffered visual.
 *
 * Return value: TRUE if the double-buffered visual is supported,
 *               FALSE otherwise.
 **/
gboolean
gdk_gl_drawable_is_double_buffered (GdkGLDrawable *gl_drawable)
{
  g_return_val_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable), FALSE);

  return GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->is_double_buffered (gl_drawable);
}

/**
 * gdk_gl_drawable_swap_buffers:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Exchange front and back buffers.
 *
 **/
void
gdk_gl_drawable_swap_buffers (GdkGLDrawable *gl_drawable)
{
  g_return_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable));

  GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->swap_buffers (gl_drawable);
}

/**
 * gdk_gl_drawable_wait_gl:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Complete OpenGL execution prior to subsequent GDK drawing calls.
 *
 **/
void
gdk_gl_drawable_wait_gl (GdkGLDrawable *gl_drawable)
{
  g_return_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable));

  GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->wait_gl (gl_drawable);
}

/**
 * gdk_gl_drawable_wait_gdk:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Complete GDK drawing execution prior to subsequent OpenGL calls.
 *
 **/
void
gdk_gl_drawable_wait_gdk (GdkGLDrawable *gl_drawable)
{
  g_return_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable));

  GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->wait_gdk (gl_drawable);
}

/**
 * gdk_gl_drawable_gl_begin:
 * @gl_drawable: a #GdkGLDrawable.
 * @gl_context: a #GdkGLContext.
 *
 * Delimits the begining of the OpenGL execution.
 *
 * Return value: TRUE if it is successful, FALSE otherwise.
 **/
gboolean
gdk_gl_drawable_gl_begin (GdkGLDrawable *gl_drawable,
			  GdkGLContext  *gl_context)
{
  g_return_val_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable), FALSE);

  return GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->gl_begin (gl_drawable,
                                                            gl_drawable,
                                                            gl_context);
}

/**
 * gdk_gl_drawable_gl_end:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Delimits the end of the OpenGL execution.
 *
 **/
void
gdk_gl_drawable_gl_end (GdkGLDrawable *gl_drawable)
{
  g_return_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable));

  GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->gl_end (gl_drawable);
}

/**
 * gdk_gl_drawable_get_gl_config:
 * @gl_drawable: a #GdkGLDrawable.
 *
 * Gets #GdkGLConfig with which the @gl_drawable is configured.
 *
 * Return value: the #GdkGLConfig.
 **/
GdkGLConfig *
gdk_gl_drawable_get_gl_config (GdkGLDrawable *gl_drawable)
{
  g_return_val_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable), NULL);

  return GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->get_gl_config (gl_drawable);
}

/**
 * gdk_gl_drawable_get_size:
 * @gl_drawable: a #GdkGLDrawable.
 * @width: location to store drawable's width, or NULL.
 * @height: location to store drawable's height, or NULL.
 *
 * Fills *width and *height with the size of GL drawable.
 * width or height can be NULL if you only want the other one.
 *
 **/
void
gdk_gl_drawable_get_size (GdkGLDrawable *gl_drawable,
                          gint          *width,
                          gint          *height)
{
  g_return_if_fail (GDK_IS_GL_DRAWABLE (gl_drawable));

  GDK_GL_DRAWABLE_GET_CLASS (gl_drawable)->get_size (gl_drawable, width, height);
}

/**
 * gdk_gl_drawable_get_current:
 *
 * Returns the current #GdkGLDrawable.
 *
 * Return value: the current #GdkGLDrawable or NULL if there is no current drawable.
 **/
GdkGLDrawable *
gdk_gl_drawable_get_current (void)
{
  GdkGLContext *gl_context;

  GDK_GL_NOTE_FUNC ();

  gl_context = gdk_gl_context_get_current ();
  if (gl_context == NULL)
    return NULL;

  return gdk_gl_context_get_gl_drawable (gl_context);
}
