/* GtkGLExt - OpenGL Extension to GTK+
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <gtk/gtkmain.h>

#include "gtkglintl.h"
#include "gtkglprivate.h"
#include "gtkgldrawingarea.h"

enum {
  INITIALIZE_GRAPHICS,
  LAST_SIGNAL
};

enum {
  PROP_0,
  PROP_GL_CONFIG,
  PROP_BACKGROUND,
  PROP_SHARE_CONTEXT,
  PROP_DIRECT,
  PROP_RENDER_TYPE
};

static void gtk_gl_drawing_area_class_init    (GtkGLDrawingAreaClass *klass);
static void gtk_gl_drawing_area_init          (GtkGLDrawingArea      *gl_darea);
static void gtk_gl_drawing_area_set_property  (GObject               *object,
                                               guint                  prop_id,
                                               const GValue          *value,
                                               GParamSpec            *pspec);
static void gtk_gl_drawing_area_get_property  (GObject               *object,
                                               guint                  prop_id,
                                               GValue                *value,
                                               GParamSpec            *pspec);
static void gtk_gl_drawing_area_finalize      (GObject               *object);
static void gtk_gl_drawing_area_realize       (GtkWidget             *widget);
static void gtk_gl_drawing_area_unrealize     (GtkWidget             *widget);
static void gtk_gl_drawing_area_size_allocate (GtkWidget             *widget,
                                               GtkAllocation         *allocation);
static void gtk_gl_drawing_area_style_set     (GtkWidget             *widget,
                                               GtkStyle              *previous_style);

static gpointer parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

static GQuark quark_gl_context = 0;

GType
gtk_gl_drawing_area_get_type (void)
{
  static GType gl_drawing_area_type = 0;

  if (!gl_drawing_area_type)
    {
      static const GTypeInfo gl_drawing_area_info =
      {
	sizeof (GtkGLDrawingAreaClass),
	NULL,		/* base_init */
	NULL,		/* base_finalize */
	(GClassInitFunc) gtk_gl_drawing_area_class_init,
	NULL,		/* class_finalize */
	NULL,		/* class_init */
	sizeof (GtkGLDrawingArea),
	0,		/* n_preallocs */
	(GInstanceInitFunc) gtk_gl_drawing_area_init,
	NULL		/* value_table */
      };

      gl_drawing_area_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
                                                     "GtkGLDrawingArea",
                                                     &gl_drawing_area_info, 0);
    }

  return gl_drawing_area_type;
}

static void
gtk_gl_drawing_area_class_init (GtkGLDrawingAreaClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass *) klass;
  widget_class = (GtkWidgetClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  quark_gl_context = g_quark_from_static_string ("gtk-gl-context");

  gobject_class->set_property = gtk_gl_drawing_area_set_property;
  gobject_class->get_property = gtk_gl_drawing_area_get_property;
  gobject_class->finalize = gtk_gl_drawing_area_finalize;

  widget_class->realize = gtk_gl_drawing_area_realize;
  widget_class->unrealize = gtk_gl_drawing_area_unrealize;
  widget_class->size_allocate = gtk_gl_drawing_area_size_allocate;
  widget_class->style_set = gtk_gl_drawing_area_style_set;

  klass->initialize_graphics = NULL;

  g_object_class_install_property (gobject_class,
                                   PROP_GL_CONFIG,
                                   g_param_spec_object ("gl_config",
                                                        _("OpenGL frame buffer configuration"),
                                                        _("The GdkGLConfig that specifies the attributes of frame buffer"),
                                                        GDK_TYPE_GL_CONFIG,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
  g_object_class_install_property (gobject_class,
                                   PROP_BACKGROUND,
                                   g_param_spec_boolean ("background",
                                                         _("Window's background"),
                                                         _("Whether the window has its background filled by the windowing system."),
                                                         FALSE,
                                                         G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class,
                                   PROP_SHARE_CONTEXT,
                                   g_param_spec_object ("share_context",
                                                        _("Rendering context which shares the display lists."),
                                                        _("A rendering context which shares the display lists with the GtkGLDrawingArea's context"),
                                                        GDK_TYPE_GL_CONTEXT,
                                                        G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class,
                                   PROP_DIRECT,
                                   g_param_spec_boolean ("direct",
                                                         _("Direct rendering context"),
                                                         _("Whether the GtkGLDrawingArea uses a direct rendering context"),
                                                         FALSE,
                                                         G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class,
                                   PROP_RENDER_TYPE,
                                   g_param_spec_enum ("render_type",
                                                      _("Render type"),
                                                      _("The type of the rendering context which is used by the GtkGLDrawingArea"),
                                                      GDK_TYPE_GL_RENDER_TYPE,
                                                      GDK_GL_RGBA_TYPE,
                                                      G_PARAM_READWRITE));

  signals[INITIALIZE_GRAPHICS] =
    g_signal_new ("initialize_graphics",
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_FIRST,
                  G_STRUCT_OFFSET (GtkGLDrawingAreaClass, initialize_graphics),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);
}

static void
gtk_gl_drawing_area_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  GtkGLDrawingArea *gl_darea = GTK_GL_DRAWING_AREA (object);

  switch (prop_id)
    {
    case PROP_GL_CONFIG:
      gtk_gl_drawing_area_set_gl_config (gl_darea, g_value_get_object (value));
      break;
    case PROP_BACKGROUND:
      gtk_gl_drawing_area_set_background (gl_darea, g_value_get_boolean (value));
      break;
    case PROP_SHARE_CONTEXT:
      gtk_gl_drawing_area_set_share_context (gl_darea, g_value_get_object (value));
      break;
    case PROP_DIRECT:
      gtk_gl_drawing_area_set_direct (gl_darea, g_value_get_boolean (value));
      break;
    case PROP_RENDER_TYPE:
      gtk_gl_drawing_area_set_render_type (gl_darea, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gtk_gl_drawing_area_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  GtkGLDrawingArea *gl_darea = GTK_GL_DRAWING_AREA (object);

  switch (prop_id)
    {
    case PROP_GL_CONFIG:
      g_value_set_object (value, gl_darea->gl_config);
      break;
    case PROP_BACKGROUND:
      g_value_set_boolean (value, gl_darea->background);
      break;
    case PROP_SHARE_CONTEXT:
      g_value_set_object (value, gl_darea->share_context);
      break;
    case PROP_DIRECT:
      g_value_set_boolean (value, gl_darea->direct);
      break;
    case PROP_RENDER_TYPE:
      g_value_set_enum (value, gl_darea->render_type);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
gtk_gl_drawing_area_init (GtkGLDrawingArea *gl_darea)
{
  GTK_WIDGET_UNSET_FLAGS (gl_darea, GTK_NO_WINDOW);

  gl_darea->gl_config = NULL;

  gl_darea->background = FALSE;

  gl_darea->share_context = NULL;
  /* gl_darea->share_widget = NULL; */
  gl_darea->direct = FALSE;
  gl_darea->render_type = GDK_GL_RGBA_TYPE;

  gl_darea->gl_window = NULL;

  /* Disable backing store feature of the widget. */
  gtk_widget_set_double_buffered (GTK_WIDGET (gl_darea), FALSE);

  /* To destroy rendering context on quit. */
  gtk_quit_add_destroy (gtk_main_level () + 1, GTK_OBJECT (gl_darea));
}

static void
gtk_gl_drawing_area_finalize (GObject *object)
{
  GtkGLDrawingArea *gl_darea = GTK_GL_DRAWING_AREA (object);

  g_object_unref (G_OBJECT (gl_darea->gl_config));
  gl_darea->gl_config = NULL;

  if (gl_darea->share_context)
    {
      g_object_unref (G_OBJECT (gl_darea->share_context));
      gl_darea->share_context = NULL;
    }

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkWidget *
gtk_gl_drawing_area_new (GdkGLConfig *gl_config)
{
  return g_object_new (GTK_TYPE_GL_DRAWING_AREA,
                       "gl_config", gl_config,
                       NULL);
}

void
gtk_gl_drawing_area_set_gl_config (GtkGLDrawingArea *gl_darea,
                                   GdkGLConfig      *gl_config)
{
  GdkColormap *colormap;

  g_return_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea));
  g_return_if_fail (!GTK_WIDGET_REALIZED (gl_darea));

  if (gl_config && gl_config == gl_darea->gl_config)
    return;

  if (!gl_config)
    {
      /* Single-buffered RGBA mode. It should succeed. */
      gl_darea->gl_config = gdk_gl_config_new_by_mode (0);
    }
  else
    {
      if (gl_darea->gl_config)
        g_object_unref (G_OBJECT (gl_darea->gl_config));

      gl_darea->gl_config = gl_config;

      g_object_ref (G_OBJECT (gl_darea->gl_config));
    }

  /* Set appropriate OpenGL-capable colormap. */
  colormap = gdk_gl_config_get_colormap (gl_darea->gl_config);
  gtk_widget_set_colormap (GTK_WIDGET (gl_darea), colormap);

  g_object_notify (G_OBJECT (gl_darea), "gl_config");
}

GdkGLConfig *
gtk_gl_drawing_area_get_gl_config (GtkGLDrawingArea *gl_darea)
{
  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), NULL);

  if (!gl_darea->gl_config)
    gtk_gl_drawing_area_set_gl_config (gl_darea, NULL);

  return gl_darea->gl_config;
}

void
gtk_gl_drawing_area_set_background (GtkGLDrawingArea *gl_darea,
                                    gboolean          background)
{
  g_return_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea));
  g_return_if_fail (!GTK_WIDGET_REALIZED (gl_darea));

  gl_darea->background = background;

  g_object_notify (G_OBJECT (gl_darea), "background");
}

gboolean
gtk_gl_drawing_area_get_background (GtkGLDrawingArea *gl_darea)
{
  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), FALSE);

  return gl_darea->background;
}

void
gtk_gl_drawing_area_set_share_context (GtkGLDrawingArea *gl_darea,
                                       GdkGLContext     *share_context)
{
  GdkGLContext *gl_context;

  g_return_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea));

  if (share_context == gl_darea->share_context)
    return;

  if (gl_darea->share_context)
    g_object_unref (G_OBJECT (gl_darea->share_context));

  gl_darea->share_context = share_context;

  if (gl_darea->share_context)
    g_object_ref (G_OBJECT (gl_darea->share_context));

  /* If rendering context is already created, destroy it. */
  gl_context = g_object_get_qdata (G_OBJECT (gl_darea), quark_gl_context);
  if (gl_context)
    g_object_set_qdata (G_OBJECT (gl_darea), quark_gl_context, NULL);

  g_object_notify (G_OBJECT (gl_darea), "share_context");
}

GdkGLContext *
gtk_gl_drawing_area_get_share_context (GtkGLDrawingArea *gl_darea)
{
  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), NULL);

  return gl_darea->share_context;
}

void
gtk_gl_drawing_area_set_direct (GtkGLDrawingArea *gl_darea,
                                gboolean          direct)
{
  GdkGLContext *gl_context;

  g_return_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea));

  if (direct == gl_darea->direct)
    return;

  gl_darea->direct = direct;

  /* If rendering context is already created, destroy it. */
  gl_context = g_object_get_qdata (G_OBJECT (gl_darea), quark_gl_context);
  if (gl_context)
    g_object_set_qdata (G_OBJECT (gl_darea), quark_gl_context, NULL);

  g_object_notify (G_OBJECT (gl_darea), "direct");
}

gboolean
gtk_gl_drawing_area_get_direct (GtkGLDrawingArea *gl_darea)
{
  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), FALSE);

  return gl_darea->direct;
}

void
gtk_gl_drawing_area_set_render_type (GtkGLDrawingArea *gl_darea,
                                     int render_type)
{
  GdkGLContext *gl_context;

  g_return_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea));

  if (render_type == gl_darea->render_type)
    return;

  gl_darea->render_type = render_type;

  /* If rendering context is already created, destroy it. */
  gl_context = g_object_get_qdata (G_OBJECT (gl_darea), quark_gl_context);
  if (gl_context)
    g_object_set_qdata (G_OBJECT (gl_darea), quark_gl_context, NULL);

  g_object_notify (G_OBJECT (gl_darea), "render_type");
}

int
gtk_gl_drawing_area_get_render_type (GtkGLDrawingArea *gl_darea)
{
  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), -1);

  return gl_darea->render_type;
}

static void
send_configure_event (GtkWidget *widget)
{
  GdkEvent *event = gdk_event_new (GDK_CONFIGURE);

  event->configure.window = g_object_ref (widget->window);
  event->configure.send_event = TRUE;
  event->configure.x = widget->allocation.x;
  event->configure.y = widget->allocation.y;
  event->configure.width = widget->allocation.width;
  event->configure.height = widget->allocation.height;
  
  gtk_widget_event (widget, event);
  gdk_event_free (event);
}

static void
gtk_gl_drawing_area_realize (GtkWidget *widget)
{
  GtkGLDrawingArea *gl_darea;
  GdkGLConfig *gl_config;
  GdkWindowAttr attributes;
  gint attributes_mask;

  gl_darea = GTK_GL_DRAWING_AREA (widget);
  gl_config = gtk_gl_drawing_area_get_gl_config (gl_darea);

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
                                   &attributes, attributes_mask);

  gdk_window_set_user_data (widget->window, gl_darea);

  widget->style = gtk_style_attach (widget->style, widget->window);

  if (gl_darea->background)
    {
      gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
    }
  else
    {
      /* Set a background of "None" on window. */
      gdk_window_set_back_pixmap (widget->window, NULL, FALSE);
    }

  /* Create OpenGL window. */
  gl_darea->gl_window = GDK_GL_DRAWABLE (gdk_gl_window_new (gl_config,
                                                            widget->window,
                                                            NULL));

  /* "initialize_graphics" signal. */
  g_signal_emit (gl_darea, signals[INITIALIZE_GRAPHICS], 0);

  /* Send configure event. */
  send_configure_event (widget);
}

static void
gtk_gl_drawing_area_unrealize (GtkWidget *widget)
{
  GtkGLDrawingArea *gl_darea;

  gl_darea = GTK_GL_DRAWING_AREA (widget);

  /* Destroy rendering context. */
  g_object_set_qdata (G_OBJECT (gl_darea), quark_gl_context, NULL);

  /* Destroy OpenGL window. */
  gdk_gl_window_destroy (GDK_GL_WINDOW (gl_darea->gl_window));
  gl_darea->gl_window = NULL;

  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
gtk_gl_drawing_area_size_allocate (GtkWidget     *widget,
                                   GtkAllocation *allocation)
{
  GtkGLDrawingArea *gl_darea;

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
                              allocation->x, allocation->y,
                              allocation->width, allocation->height);

      /* Synchronize OpenGL and window resizing request streams. */
      gl_darea = GTK_GL_DRAWING_AREA (widget);
      gdk_gl_drawable_wait_gdk (gl_darea->gl_window);

      /* Send configure event. */
      send_configure_event (widget);
    }
}

static void
gtk_gl_drawing_area_style_set (GtkWidget *widget,
                               GtkStyle  *previous_style)
{
  GtkGLDrawingArea *gl_darea;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gl_darea = GTK_GL_DRAWING_AREA (widget);
      if (gl_darea->background)
        gtk_style_set_background (widget->style, widget->window, widget->state);
    }
}

GdkGLContext *
gtk_gl_drawing_area_create_gl_context (GtkGLDrawingArea *gl_darea,
                                       GdkGLContext     *share_list,
                                       gboolean          direct,
                                       int               render_type)
{
  GdkGLContext *gl_context;

  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), NULL);
  g_return_val_if_fail (GTK_WIDGET_REALIZED (gl_darea), NULL);

  gl_context = gdk_gl_context_new (gl_darea->gl_window,
                                   share_list,
                                   direct,
                                   render_type);
  if (!gl_context)
    {
      g_warning (G_STRLOC ": cannot create GdkGLContext");
      return NULL;
    }

  return gl_context;
}

GdkGLContext *
gtk_gl_drawing_area_get_gl_context (GtkGLDrawingArea *gl_darea)
{
  GdkGLContext *gl_context;

  g_return_val_if_fail (GTK_IS_GL_DRAWING_AREA (gl_darea), NULL);
  g_return_val_if_fail (GTK_WIDGET_REALIZED (gl_darea), NULL);

  gl_context = g_object_get_qdata (G_OBJECT (gl_darea), quark_gl_context);
  if (!gl_context)
    {
      gl_context = gtk_gl_drawing_area_create_gl_context (gl_darea,
                                                          gl_darea->share_context,
                                                          gl_darea->direct,
                                                          gl_darea->render_type);
      if (gl_context)
        {
          g_object_set_qdata_full (G_OBJECT (gl_darea),
                                   quark_gl_context,
                                   gl_context,
                                   g_object_unref);
                                /* (GDestroyNotify) gdk_gl_context_destroy); */
        }
    }

  return gl_context;
}
