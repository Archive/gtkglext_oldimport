/* GtkGLExt - OpenGL Extension to GTK+
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#ifndef __GTK_GL_DRAWING_AREA_H__
#define __GTK_GL_DRAWING_AREA_H__

#include <gdk/gdk.h>

#include <gdk/gdkgl.h>

#include <gtk/gtkdrawingarea.h>

G_BEGIN_DECLS

#define GTK_TYPE_GL_DRAWING_AREA            (gtk_gl_drawing_area_get_type ())
#define GTK_GL_DRAWING_AREA(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_GL_DRAWING_AREA, GtkGLDrawingArea))
#define GTK_GL_DRAWING_AREA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_GL_DRAWING_AREA, GtkGLDrawingAreaClass))
#define GTK_IS_GL_DRAWING_AREA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_GL_DRAWING_AREA))
#define GTK_IS_GL_DRAWING_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_GL_DRAWING_AREA))
#define GTK_GL_DRAWING_AREA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_GL_DRAWING_AREA, GtkGLDrawingAreaClass))

typedef struct _GtkGLDrawingArea      GtkGLDrawingArea;
typedef struct _GtkGLDrawingAreaClass GtkGLDrawingAreaClass;

struct _GtkGLDrawingArea
{
  GtkDrawingArea drawing_area;

  /*< private >*/

  GdkGLConfig *gl_config;

  gboolean background;

  GdkGLContext *share_context;
  /* GtkGLDrawingArea *share_widget; */
  gboolean direct;
  int render_type;

  /*< public >*/

  GdkGLDrawable *gl_window;
};

struct _GtkGLDrawingAreaClass
{
  GtkDrawingAreaClass parent_class;

  void (* initialize_graphics) (GtkGLDrawingArea *gl_darea);
  
  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};

GType         gtk_gl_drawing_area_get_type (void) G_GNUC_CONST;

GtkWidget    *gtk_gl_drawing_area_new (GdkGLConfig *gl_config);

void          gtk_gl_drawing_area_set_gl_config (GtkGLDrawingArea *gl_darea,
                                                 GdkGLConfig      *gl_config);
GdkGLConfig  *gtk_gl_drawing_area_get_gl_config (GtkGLDrawingArea *gl_darea);

void          gtk_gl_drawing_area_set_background (GtkGLDrawingArea *gl_darea,
                                                  gboolean          background);
gboolean      gtk_gl_drawing_area_get_background (GtkGLDrawingArea *gl_darea);

void          gtk_gl_drawing_area_set_share_context (GtkGLDrawingArea *gl_darea,
                                                     GdkGLContext     *share_context);
GdkGLContext *gtk_gl_drawing_area_get_share_context (GtkGLDrawingArea *gl_darea);

void          gtk_gl_drawing_area_set_direct (GtkGLDrawingArea *gl_darea,
                                              gboolean          direct);
gboolean      gtk_gl_drawing_area_get_direct (GtkGLDrawingArea *gl_darea);

void          gtk_gl_drawing_area_set_render_type (GtkGLDrawingArea *gl_darea,
                                                   int render_type);
int           gtk_gl_drawing_area_get_render_type (GtkGLDrawingArea *gl_darea);

GdkGLContext *gtk_gl_drawing_area_create_gl_context (GtkGLDrawingArea *gl_darea,
                                                     GdkGLContext     *share_list,
                                                     gboolean          direct,
                                                     int               render_type);

GdkGLContext *gtk_gl_drawing_area_get_gl_context (GtkGLDrawingArea *gl_darea);

G_END_DECLS

#endif /* __GTK_GL_DRAWING_AREA_H__ */
